# -*- coding: utf-8 -*-
#
# Connect CH-250 through seriual port via FTDI or top RPI
# Set serial port in code, 9600 baud no flow control
#
#
#
import pygame
import serial
import time

pygame.init
pygame.font.init() # you have to call this at the start, 

myfont = pygame.font.SysFont(None, 48)

win = pygame.display.set_mode ((1200, 700))
pygame.display.set_caption ("Chemtrol CH-250")


ser = serial.Serial(
   #port = 'COM38',
   port = '/dev/serial0',
   baudrate = 9600,
   parity = serial.PARITY_NONE,
   stopbits = serial.STOPBITS_ONE,
   bytesize = serial.EIGHTBITS,
   timeout = 4
   )
   
ser.close()
ser.open()
S = 0
CH250_Stat = 0

if ser.isOpen():
   print ("Serial is open!")
   
ORP_FEED  = (83, 193)
ORP_ALARM = (83, 330)
ORP_OFF   = (175, 420)
ORP_MAN   = (305, 420)
ORP_AUTO  = (435, 420)
PH_OFF    = (695, 420)
PH_MAN    = (825, 420)
PH_AUTO   = (950, 420)
PH_ALARM  = (1050, 330)
PH_FEED   = (1050, 193)

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)

Orp = 'xxx'
pH = 'xx'
Status = '.'
Statusi = 0

#bg = pygame.image.load('CH-250-Panel-1200-700.png')
#char = pygame.image.load('standing.png')

# Set up the background image.
bg = pygame.image.load('Panel.png')
# Use smoothscale() to stretch the board image to fit the entire board:
bg = pygame.transform.smoothscale(bg, (1200, 700))


clock = pygame.time.Clock ( )
x = 50
y = 400
width = 64
height = 64


left = False
right = False

isJump = False

#text = font.render("Hello, World", True, (0, 128, 0))

def text_objects (text, font):
   textSurface = font.render (text, True, RED)
   return textSurface, textSurface.get_rect()

def message_display(text):
    largeText = pygame.font.Font('freesansbold.ttf',115)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((600),(350))
    win.blit (TextSurf, TextRect)
    #time.sleep (2)

#ORP_SIZE = [240, 135]
#PH_SIZE  = [165, 135]
    
def ORP_display(text):
    largeText = pygame.font.Font('freesansbold.ttf',115)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((335),(232))
    win.blit (TextSurf, TextRect)
    # print (text)
    
def PH_display(text):
    largeText = pygame.font.Font('freesansbold.ttf',115)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((852),(232))
    win.blit (TextSurf, TextRect)
   # print (text)

def Status_display (S):
   if (S & 64) != 0:
      win.blit (pygame.image.load ('GREEN.png'),ORP_FEED)
   else:
      win.blit (pygame.image.load ('BLUE.png'),ORP_FEED)

   if (S & 128) != 0:
      win.blit (pygame.image.load ('RED.png'),ORP_ALARM)
   else:
      win.blit (pygame.image.load ('BLUE.png'),ORP_ALARM)

   if (S & 4) != 0:
      win.blit (pygame.image.load ('GREEN.png'),PH_FEED)
   else:
      win.blit (pygame.image.load ('BLUE.png'),PH_FEED)

   if (S & 8) != 0:
      win.blit (pygame.image.load ('RED.png'),PH_ALARM)
   else:
      win.blit (pygame.image.load ('BLUE.png'),PH_ALARM)
      
   win.blit (pygame.image.load ('BLUE.png'),ORP_OFF)
   win.blit (pygame.image.load ('BLUE.png'),ORP_MAN)
   win.blit (pygame.image.load ('BLUE.png'),ORP_AUTO)
   if (S & 16) == 0 and (S & 32) == 0:
      win.blit (pygame.image.load ('YELLOW.png'),ORP_OFF)
   if (S & 16) != 0 and (S & 32) == 0:
      win.blit (pygame.image.load ('YELLOW.png'),ORP_MAN)
   if (S & 16) == 0 and (S & 32) != 0:
      win.blit (pygame.image.load ('YELLOW.png'),ORP_AUTO)

   win.blit (pygame.image.load ('BLUE.png'),PH_OFF)
   win.blit (pygame.image.load ('BLUE.png'),PH_MAN)
   win.blit (pygame.image.load ('BLUE.png'),PH_AUTO)
   if (S & 1) == 0 and (S & 2) == 0:
      win.blit (pygame.image.load ('YELLOW.png'),PH_OFF)
   if (S & 1) != 0 and (S & 2) == 0:
      win.blit (pygame.image.load ('YELLOW.png'),PH_MAN)
   if (S & 1) == 0 and (S & 2) != 0:
      win.blit (pygame.image.load ('YELLOW.png'),PH_AUTO)

      
       
def redrawGameWindow ( ):
    global pH, Orp
    #win.fill((0, 0, 0))
    win.blit (bg, (0,0))
   # pygame.draw.rect (win, (255, 0, 0), (x, y, width, height))
        
   # if Status & 64 == 0:
   # win.blit (pygame.image.load ('BLUE.png'),ORP_FEED)
   # if Status & 64 != 0:
   # win.blit (pygame.image.load ('YELLOW.png'),ORP_FEED)
    #elif CH250_Stat == 1:
    #win.blit (pygame.image.load ('RED.png'),ORP_ALARM)
    #elif CH250_Stat == 2:
   #win.blit (pygame.image.load ('GREEN.png'),ORP_OFF)
    #elif CH250_Stat == 3:
   # win.blit (pygame.image.load ('YELLOW.png'),ORP_MAN)
   # win.blit (pygame.image.load ('BLUE.png'),ORP_AUTO)

   # win.blit (pygame.image.load ('RED.png'),PH_OFF)
    #win.blit (pygame.image.load ('GREEN.png'),PH_MAN)
   # win.blit (pygame.image.load ('YELLOW.png'),PH_AUTO)
   # win.blit (pygame.image.load ('RED.png'),PH_ALARM)
    #win.blit (pygame.image.load ('BLUE.png'),PH_FEED)

  

   #pygame.draw.rect (win, (255, 0, 0), (x, y, width, height))
    ORP_UL = [215, 165]
    ORP_UR = [455, 165]
    ORP_LL = [215, 300]
    ORP_LR = [455, 300]
    pygame.draw.lines(win, RED, False, [ORP_UL, ORP_UR, ORP_LR, ORP_LL, ORP_UL], 3)
    
    PH_UL = [770, 165]
    PH_UR = [935, 165]
    PH_LL = [770, 300]
    PH_LR = [935, 300]
    pygame.draw.lines(win, RED, False, [PH_UL, PH_UR, PH_LR, PH_LL, PH_UL], 3)

    PH_display (pH)
    ORP_display (Orp)
    Status_display (Statusi)
#168 prints dots

    pygame.display.update ( )

def getData (S):
      global pH, Orp, Statusi
      #time.sleep (1)
      if S == 0:
         ser.write (("*IDN?\r\n").encode ("utf8"))
        
      if S == 1:
        ser.write ((":pH?\r\n").encode ("utf8"))
           
      if S == 2:
        ser.write ((":ORP?\r\n").encode ("utf8"))
         
      if S == 3:
        ser.write ((":Status?\r\n").encode ("utf8"))
        
      pygame.time.delay (500)
        
      B = ser.readline ( )
      
      if S == 0:
         print (B.strip())   
      if S == 1:
        B = B.strip()
        if len(B)>2:
           print (pH)
           pH = B
           return
      if S == 2:
        B = B.strip()
        if len(B) > 2:
           Orp = B
           print (Orp)
           return
     
      if S == 3:
        Status = B.strip()
        print (Status)
        Statusi = int (Status)
        #print (Statusi)
        return

#Main Loop
run = True
redrawGameWindow ( )
while run:
    pygame.time.delay (500)
    clock.tick (500)
    for event in pygame.event.get ( ):
        if event.type == pygame.QUIT:
            run = False
    
    getData (1)
    pygame.time.delay (500)
    
    getData (2)
    pygame.time.delay (100)
    #getData (3))
    Statusi = 34
    
    keys = pygame.key.get_pressed ( )

    redrawGameWindow ( )
             
ser.close()    
pygame.quit ( )


#print ("Hello Python World")
#if __name__ == "__main__":
    #import unittest
    

